﻿using HW4.Models;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System;
using System.Collections.Generic;

namespace HW4
{
    public partial class MoreStudentInfo : ContentPage
    {
        string LinkPage;
        public MoreStudentInfo()
        {
            InitializeComponent();
        }

        public MoreStudentInfo(CellItem CellItem)
        {
            InitializeComponent();

            BindingContext = CellItem;

            LinkPage = CellItem.url;
        }
        void HandleOnClick(object sender, System.EventArgs e)
        {
            var uri = new Uri(LinkPage);
            Device.OpenUri(uri);
        }
    }
}